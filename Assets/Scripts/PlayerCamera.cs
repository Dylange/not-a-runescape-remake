﻿using UnityEngine;
using System.Collections;

public class PlayerCamera : MonoBehaviour {

    public float MaxFollowDistance;
    public float Height;
    public float LocalRotOffset;
    public float LocalHeightOffset;
    public GameObject Player;

    // Use this for initialization
    void Start () {
        MaxFollowDistance = Vector3.Distance(transform.position, Player.transform.position);

    }
	
	// Update is called once per frame
	void Update () {
	    
	}

    public void Move(float forwardAmount) {
        Debug.Log("PlayerRot:" + Player.GetComponent<Player>().RotationOffset + "///CameraRot:" + LocalRotOffset);
        GetLeftMouseInput();
        LocalHeightOffset = Mathf.Clamp(LocalHeightOffset, Height * -1, Height * 2);
        
        transform.position += Player.transform.forward * forwardAmount;
        transform.position = new Vector3(transform.position.x, Mathf.Clamp(Height - LocalHeightOffset, Height - 14.6f, Height * 2f), transform.position.z);
        transform.LookAt(Player.transform);
    }

    void GetLeftMouseInput() {
        float rotationOffset = Player.GetComponent<Player>().RotationOffset;
        if (Input.GetMouseButton(0)) {//Holding left mouse button - rotate camera view
            float mouseDX = Input.GetAxis("Mouse X");
            LocalRotOffset -= mouseDX * 4f;
            float mouseDY = Input.GetAxis("Mouse Y");
            LocalHeightOffset += mouseDY;
            transform.RotateAround(Player.transform.position, Vector3.up, rotationOffset - LocalRotOffset);
        } else {
            if (rotationOffset < LocalRotOffset - 0.1f) {
                LocalRotOffset = Mathf.Lerp(LocalRotOffset, rotationOffset, 0.1f);
            } else if (rotationOffset > LocalRotOffset + 0.1f) {
                LocalRotOffset = Mathf.Lerp(LocalRotOffset, rotationOffset, 0.1f);
            }
            if (LocalHeightOffset > 0.1 || LocalHeightOffset < -0.1) {
                LocalHeightOffset = Mathf.Lerp(LocalHeightOffset, 0f, 0.1f);
            }
        }
    }

}
