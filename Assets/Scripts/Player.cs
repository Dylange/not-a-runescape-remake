﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    public float MOVEMENT_CONSTANT;
    public float ROTATION_CONSTANT;
    public float CAMERA_MAX_HEIGHT;
    public GameObject CameraTarget;
    public Camera PlayerCamera;

    private float BACKWARD_SPEED_MULTIPLIER = 0.5f;
    public float RotationOffset = 0f;

    private Animator anim = null;

	// Use this for initialization
	void Start () {
        anim = GetComponent<Animator>();
        CAMERA_MAX_HEIGHT = PlayerCamera.transform.position.y;

    }
	
	// Update is called once per frame
	void Update () {

        if (Input.GetKeyDown(KeyCode.Space)) {
            anim.SetTrigger("jump");
        }

        float animDZ = anim.GetFloat("dz");
        anim.SetFloat("dz", Input.GetAxis("Vertical"));//set running/walking animation accordingly

        float dx = Input.GetAxis("Horizontal");
        float dz = Input.GetAxis("Vertical");

        float extraMouseRot = 0f;//handle extra rotation with mouse right click drag
        if (Input.GetMouseButton(1)) {
            float mouseDX = Input.GetAxis("Mouse X") * 3;
            transform.Rotate(0f, mouseDX, 0f);
            extraMouseRot = mouseDX;
        }

        if(dz < 0f) {//move slower backwards
            dz *= BACKWARD_SPEED_MULTIPLIER;
        }


        PlayerCamera camScript = PlayerCamera.GetComponent<PlayerCamera>();
        transform.position += transform.forward * MOVEMENT_CONSTANT * dz * Time.deltaTime;
        transform.Rotate(0f, ROTATION_CONSTANT * dx * Time.deltaTime, 0f);
        RotationOffset += extraMouseRot + (ROTATION_CONSTANT * dx * Time.deltaTime);

        camScript.Height = transform.position.y + CAMERA_MAX_HEIGHT;
        camScript.Move(MOVEMENT_CONSTANT * dz * Time.deltaTime);
        /*if (Input.GetMouseButton(0)) {
            PlayerCamera.transform.RotateAround(transform.position, Vector3.up, rotationOffset - camScript.LocalRotOffset);
        }*/
        PlayerCamera.transform.LookAt(CameraTarget.transform.position);

    }
}
