﻿using UnityEngine;
using System.Collections;

public class Skills : MonoBehaviour {

    public const int HP = 0,
        ATTACK = 1,
        STRENGTH = 2,
        DEFENCE = 3,
        WOODCUTTING = 4,
        FISHING = 5,
        MINING = 6,
        HUNTER = 7;

    public float[] experience = {0f,
                                0f,
                                0f,
                                0f,
                                0f,
                                0f,
                                0f,
                                0f};

    public int[] levels = {1,
                            1,
                            1,
                            1,
                            1,
                            1,
                            1,
                            1};

    void FixedUpdate() {
        
    }

    public int getXpForLevel(int lvl) {
        float total = 0;
        for (int i = 1; i < lvl; i++) {
            total += i + 300 * Mathf.Pow(2, i / 7.0f);
        }
        return (int)((total * 2.5f) / 10f);
    }

    public float getXp(int index) {
        return this.experience[index];
    }

    public int getLevel(int index) {
        return this.levels[index];
    }

    public void addXp(int index, int xp) {
        experience[index] += xp;
        if(experience[index] > getXpForLevel(levels[index] + 1)) {
            levels[index]++;
        }
    }

    public void setLevel(int index, int lvl) {
        levels[index] = lvl;
        experience[index] = getXpForLevel(lvl);
    }

}
